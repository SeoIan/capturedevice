﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaptureDeviceExam.CaptureDevice
{
    abstract class CaptureDevice 
    {
        public CaptureDevice();
        public ~CaptureDevice();

        public abstract bool open();
        public abstract bool close();
        public abstract bool acquisitionStart();
        public abstract bool acquisitionStop();
        public abstract bool width();
        public abstract bool height();



    }
}
